class FileUpload
  include Mongoid::Document
  include Mongoid::Timestamps # adds created_at and updated_at fields

  # field <name>, :type => <type>, :default => <value>
  field :name, :type => String
  field :url, :type => String
  field :content_type, :type => String
  field :file_size, :type => Integer
  field :original_name, :type => String
  field :ext_name, :type => String
  field :model_name, :type => String

  # You can define indexes on documents using the index macro:
  # index :field <, :unique => true>

  # You can create a composite key in mongoid to replace the default id using the key macro:
  # key :field <, :another_field, :one_more ....>
  class << self
    def upload(url,m=nil,opt={})
      if Padrino.env == :production
        if Setting.site_domain
          url = "//#{Setting.site_domain}#{url}"        
        end
      end
     if Setting.default_image_size
        url = "#{url}/#{Setting.default_image_size}#{File.extname(url)}"
      end
      create({name:File.basename(url),url:url,content_type:opt[:type],
        original_name:opt[:filename],ext_name:File.extname(url),model_name:m})      
    end
  end
end
