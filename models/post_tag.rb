class PostTag
  include Mongoid::Document
  include Mongoid::Timestamps # adds created_at and updated_at fields

  # field <name>, :type => <type>, :default => <value>
  # field :tag, :reference => true
  # field :post, :reference => true
  belongs_to :post
  belongs_to :tag

  after_save :set_tag_num

  def set_tag_num
    self.tag.update_attribute(:num , self.tag.post_tags.size)
    # self.tag.save!
  end
  # You can define indexes on documents using the index macro:
  # index :field <, :unique => true>

  # You can create a composite key in mongoid to replace the default id using the key macro:
  # key :field <, :another_field, :one_more ....>
end
