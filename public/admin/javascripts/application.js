!function($) {
  'use strict';

  $(function() {
    function toggleAction(selector, disabled) {
      var method = disabled ? 'addClass' : 'removeClass';
      $(selector)[method]('list-menu-link-disabled').parent()[method]('list-menu-wrapper-disabled');
    }
    // Check/uncheck all functionality
    function checkAll(base, checked) {
      // Toggle all checkboxes on the table's body that exist on the first column.
      base.find(listCheckboxesSelector).prop('checked', checked);
      base.find('.list-row')[checked ? 'addClass' : 'removeClass']('list-row-selected');
      toggleAction('#delete-selected', !checked);
    }
    function generalToggle() {
      var checked = listCheckboxes.filter(':checked').length;
      toggleAction('#delete-selected', checked === 0);
      toggleAction('#deselect-all', checked === 0);
      toggleAction('#select-all', checked === listCheckboxesLength);
    }

    var listCheckboxesSelector = '.list-selectable-checkbox', list = $('#list'), alertTimeout = 4000, listCheckboxes, listCheckboxesLength;

    // Automatically close alerts if there was any present.
    if ($('.alert').length > 0) {
      setTimeout(function() { $('.alert').alert('close'); }, alertTimeout);
    }

    // Only process list-related JavaScript if there's a list!
    if (list.length > 0) {
      listCheckboxes = list.find(listCheckboxesSelector);
      listCheckboxesLength = listCheckboxes.length;
      
      // Confirm before deleting one item
      $('.list-row-action-delete-one').on('click', function(ev) {
        ev.preventDefault();
        $(this).addClass('list-row-action-wrapper-link-active')
          .siblings('.list-row-action-popover-delete-one').first().show()
          .find('.cancel').on('click', function() {

            $(this).parents('.list-row-action-popover-delete-one').hide()
              .siblings('.list-row-action-delete-one').removeClass('list-row-action-wrapper-link-active');
          });
      });

      // Select/deselect record on row's click
      list.find('.list-row').on('click', function(ev) {
        var checkbox, willBeChecked;
        ev.stopPropagation();

        if (ev.currentTarget.tagName == 'TR') { 
          checkbox = $(this).find('.list-selectable-checkbox');
          willBeChecked = !checkbox.prop('checked');
          checkbox.prop('checked', willBeChecked);
          $(this)[willBeChecked ? 'addClass' : 'removeClass']('list-row-selected');
          generalToggle();
        }
      });
      // Select all action 
      $('#select-all').on('click', function(ev) {
        ev.preventDefault();
        ev.stopPropagation();
        if ($(this).is('.list-menu-link-disabled')) return;

        // We assume we want to stay on the dropdown to delete all perhaps
        ev.stopPropagation();
        checkAll(list, true);
        toggleAction('#select-all', true);
        toggleAction('#deselect-all', false);
      });
      // Deselect all action 
      $('#deselect-all').on('click', function(ev) {
        ev.preventDefault();
        if ($(this).is('.list-menu-link-disabled')) return;

        checkAll(list, false);
        toggleAction('#deselect-all', true);
        toggleAction('#select-all', false);
      });
      // Delete selected
      $('#delete-selected').on('click', function(ev) {
        ev.preventDefault();
        ev.stopPropagation();
        if ($(this).is('.list-menu-link-disabled')) return;

        // Open the popup to confirm deletion
        $(this).parent().addClass('active').parent('.dropdown').addClass('open');
        $(this).addClass('active')
          .siblings('.list-menu-popover-delete-selected').first().show()
          .find('.cancel').on('click', function() {
          
            // Hide the popover on cancel
            $(this).parents('.list-menu-popover-delete-selected').hide()
              .siblings('#delete-selected').removeClass('active').parent().removeClass('active');
            // and close the dropdown
            $(this).parents('.dropdown').removeClass('open');
          });

        $(this).siblings('.list-menu-popover-delete-selected').find(':hidden[data-delete-many-ids=true]').
          val(listCheckboxes.filter(':checked').map(function() { return $(this).val(); }).toArray().join(','));
      });

      // Catch checkboxes check/uncheck and enable/disable the delete selected functionality
      listCheckboxes.on('click', function(ev) {
        ev.stopPropagation();

        $(this).parent('.list-row')[$(this).is(':checked') ? 'addClass' : 'removeClass']('list-row-selected');

        generalToggle();
      });
    }

    // Autofocus first field with an error. (usability)
    $('.has-error :input').first().focus();

    $("#markdown-preview").on("click",function(){
      var content = $("#markdown-content").val();
      $.post(admin_root + "posts/preview",
        {"content":content,"authenticity_token":authenticity_token},
        function(data){
          $(".markdown-preview").html(data.content);
          $(".markdown-textarea").hide();
          $(".markdown-preview").show();
          $("#markdown-preview").hide();
          $("#markdown-edit").show();

        },"json");
    });
    $("#markdown-edit").on("click",function(){
      $(".markdown-textarea").show();
      $(".markdown-preview").hide();
      $("#markdown-preview").show();
      $("#markdown-edit").hide();
    })

    $('#file_upload').fileUpload(
    {
      url: admin_root + 'posts/upload?authenticity_token='+authenticity_token,
      type: 'POST',
      dataType: 'json',
      // data:{"authenticity_token":authenticity_token},
      beforeSend: function () {
        // $(document.body).addClass('uploading');
        console.log("b")
      },
      complete: function () {
        console.log("c");
        // $(document.body).removeClass('uploading');
      },
      success: function (result, status, xhr) {
        if (!result) {
          window.alert('Server error.');
          return;
        }
        var src = '!['+result.original_name+']('+result.url+')';

        $("#markdown-content").focus();//
        if(document.selection){
          var s = document.selection.createRange();
          s.text = src;
          s.select();
        }else if($("#markdown-content")[0].selectionStart || $("#markdown-content")[0].selectionStart == 0){
          var startP = $("#markdown-content")[0].selectionStart  ; //开始光标          
          var endP   = $("#markdown-content")[0].selectionEnd  ; //开始光标
          var scrollP = $("#markdown-content")[0].scrollTop ; // 滚动位置
          $("#markdown-content").val($("#markdown-content").val().substring(0,startP) + src +$("#markdown-content").val().substring(endP));
          if(scrollP > 0){
            $("#markdown-content")[0].scrollTop  = scrollP;
          }
        }else{
          $("#markdown-content").val($("#markdown-content").val() + src);
        }
      }
    });
  });
}(window.jQuery);
