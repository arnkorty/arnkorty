class Setting
  include Mongoid::Document
  include Mongoid::Timestamps # adds created_at and updated_at fields

  # attr_accessor :
  # field <name>, :type => <type>, :default => <value>
  field :name, :type => String
  field :value, :type => String
  # 0 => normal text
  # 1 => normal markdown
  field :type_name,:type => Integer,default:0

  # You can define indexes on documents using the index macro:
  # index :field <, :unique => true>

  # You can create a composite key in mongoid to replace the default id using the key macro:
  # key :field <, :another_field, :one_more ....>
  def type_name_str
    case type_name
    when 0
      "text"
    when 1
      "markdown"
    else
      ""
    end
  end

  validates_uniqueness_of :name

  class << self

    def method_missing(method)
      v = instance_variable_get "@#{method.to_s}" 
      if v
        v
      else
        if sv = where(name: method.to_s).first 
          v = sv.value
        end
        instance_variable_set("@#{method.to_s}",v)
      end
    end
  end

end
