class Post
  include Mongoid::Document
  include Mongoid::Timestamps # adds created_at and updated_at fields

  # field <name>, :type => <type>, :default => <value>
  field :title,:type=>String
  field :content, :type => String
  field :type_name,:type=>String
  field :is_delete, :type => Boolean

  has_many :post_tags
  # has_many :tags,:through=> :post_tag

  # You can define indexes on documents using the index macro:
  # index :field <, :unique => true>
  attr_accessor :tags
  # You can create a composite key in mongoid to replace the default id using the key macro:
  # key :field <, :another_field, :one_more ....>
  after_save :set_tags

  class << self
    def find_by_tag(tag)
      return desc(:created_at) if tag.blank?
      tag = Tag.where(name:tag).first
      tag.post_tags.desc(:created_at).map{|t|t.post}
      # PostTag.where
    end
  end

  def prev
    Post.where(:created_at.lt => self.created_at).desc(:created_at).first
  end

  def next
    Post.where(:created_at.gt => self.created_at).asc(:created_at).first
  end

  def set_tags
    PostTag.delete_all(post_id: self.id)
    self.tags.split(/\s/).uniq.each do |tag|
      unless tag.blank?
        obj = Tag.where(downcase_name:tag.downcase).first || Tag.create({name:tag,downcase_name:tag.downcase})
        # obj = Tag.where(downcase_name:tag.downcase).create.update(name:tag)!
        PostTag.create({tag_id:obj.id,post_id:self.id})
      end
    end
  end

  def tags
    @tags ||= begin
      if self.id.blank?
        ""
      else
        # str = ''
        post_tags.map{|t|t.tag.name}.join(" ")
      end
    end
  end

end
