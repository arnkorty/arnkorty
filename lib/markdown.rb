require 'redcarpet'
require  'pygments'
# require 'rouge/plugins/redcarpet'
class MDToHtml < Redcarpet::Render::HTML
  # include Rouge::Plugins::Redcarpet # yep, that's it.

     def initialize(extensions={})
        super(extensions.merge(:xhtml => true,
                               :no_styles => true,
                               :filter_html => true,
                               :hard_wrap => true))
      end

      
     #  def block_code(code, language)
     #    language.downcase! if language.is_a?(String)
     #    html = super(code, language)
     #    # 将最后行的 "\n\n" 替换成回 "\n", rouge 0.3.2 的 Bug 导致
     #    html.gsub!("\n</pre>", "</pre>")
     #   html
     # end
  def block_code(code, language)
    Pygments.highlight(code, :lexer => language)
  end

  def self.arnkortyconverthtml(content,option={})
    m = self.new(option)
    md = Redcarpet::Markdown.new(
     m,
    :fenced_code_blocks => true,
    :autolink => true
    )
    md.render(content).html_safe
  end

end