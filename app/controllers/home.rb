# encoding: utf-8
Arnkorty::App.controllers :home do
  
  # get :index, :map => '/foo/bar' do
  #   session[:foo] = 'bar'
  #   render 'index'
  # end

  # get :sample, :map => '/sample/url', :provides => [:any, :js] do
  #   case content_type
  #     when :js then ...
  #     else ...
  # end

  # get :foo, :with => :id do
  #   'Maps to url '/foo/#{params[:id]}''
  # end

  # get '/example' do
  #   'Hello world!'
  # end
  get :about,:map=>"/about" do
    render "home/about"
  end
  get :index,:map=>"/" do
    # Encoding.default_internal = nil
     render "home/index"
  end

end
