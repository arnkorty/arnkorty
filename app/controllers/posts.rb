Arnkorty::App.controllers :posts do
  
  # get :index, :map => '/foo/bar' do
  #   session[:foo] = 'bar'
  #   render 'index'
  # end

  # get :sample, :map => '/sample/url', :provides => [:any, :js] do
  #   case content_type
  #     when :js then ...
  #     else ...
  # end

  # get :foo, :with => :id do
  #   'Maps to url '/foo/#{params[:id]}''
  # end

  # get '/example' do
  #   'Hello world!'
  # end

  before do 
    @recent_posts = Post.desc(:created_at).limit(10)
    @tags = Tag.desc(:num)
  end

  # get "/posts/index" do
  #   # if params[:tag].blank?
  #     redirect_to url_for(:posts,:index)
  #   # else
  #     # redirect_to "/posts?tag="+params[:tag]
  #   # end
  # end

  get :index do
    @posts = Post.find_by_tag(params[:tag])
    # @posts = @posts.
    render "/posts/index",:collection => @posts
  end

  # get "/posts/query" do
  # end

  get :show ,:with=>:id do
    @post = Post.find(params[:id])
    @title = @post.title
    render "/posts/show",:object=>@post
  end

  get :uploads,:map => '/uploads/:m/:upload_url/:opt' do
    image_ext = /jpg|png|jpeg|gif/
    ext_name = File.extname(params[:upload_url])
    base_name = params[:upload_url].gsub(ext_name,'')
    origin_path = File.join(Padrino.root,'public','uploads',params[:m],params[:upload_url])    

    if ext_name.downcase =~ image_ext
      opt_path    = File.join(File.dirname(origin_path),base_name + params[:opt] )
      # return File.dirname(opt_path),base_name#File.basename(params[:upload_url]) 
      unless File.exist? opt_path      
        image = MiniMagick::Image.open(origin_path)
        image.resize(params[:opt].split('.')[0])
        image.write(opt_path)
      end
      send_file opt_path,
                :type => "image/#{ext_name.sub('.','')}"
    else
      send_file origin_path,
                :type=>'application/octet-stream'
    end  
  end


end
