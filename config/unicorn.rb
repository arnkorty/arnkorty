module Padrino
  class <<self
    def root
      File.expand_path("../..", __FILE__)
    end
    def env
      ENV['PADRINO_ENV'] || :production
    end
  end
end

# puts Padrino.env

preload_app true
working_directory Padrino.root
pid "#{Padrino.root}/tmp/pids/unicorn.pid"
stderr_path "#{Padrino.root}/log/unicorn.error.log"
stdout_path "#{Padrino.root}/log/unicorn.log"

listen 7000, :tcp_nopush => false

listen "/tmp/unicorn.arnkorty.sock"
worker_processes 2
timeout 120


before_exec do |server|
  ENV["BUNDLE_GEMFILE"] = "#{Padrino.root}/Gemfile"
end

before_fork do |server, worker|
  old_pid = "#{Padrino.root}/tmp/pids/unicorn.pid.oldbin"
  if File.exists?(old_pid) && server.pid != old_pid
    begin
      Process.kill("QUIT", File.read(old_pid).to_i)
    rescue Errno::ENOENT, Errno::ESRCH
      puts "Send 'QUIT' signal to unicorn error!"
    end
  end
end